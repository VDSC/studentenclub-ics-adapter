# Studentenclub iCalendar Adapter

Some of Dresden's Studentenclubs do not provide their events in
[iCal format](https://www.rfc-editor.org/rfc/rfc5545). [ics.py](./ics.py)
scrapes their websites for events and generates ICS files from them.

*ics.py* deliberately has not external dependencies to allow for easy deployment.

## Demo Instance

<https://www.exmatrikulationsamt.de/downloads/schnusch/ics/>

## License

[GNU Affero General Public License v3.0 or later](./COPYING.md)
